require 'sidekiq/web'

Rails.application.routes.draw do
  draw :madmin
  get '/privacy', to: 'home#privacy'
  get '/terms', to: 'home#terms'
authenticate :user, lambda { |u| u.admin? } do
  mount Sidekiq::Web => '/sidekiq'

  namespace :madmin do
    resources :impersonates do
      post :impersonate, on: :member
      post :stop_impersonating, on: :collection
    end
  end
end

  # resources :notifications, only: [:index]
  # resources :announcements, only: [:index]
  resources :posts, except: [:new]
  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  devise_scope :user do
    get '/users/:id/profile' => 'users#profile', :as => 'user_profile'
  end
  devise_scope :announcements do
    get '/announcements/:id' => 'announcements#show', :as => 'announcements_post'
  end
  root to: 'home#index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
