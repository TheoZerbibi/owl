module BannerHelper
  def banner_path(object)
    if object.respond_to?(:banner) && object.banner.attached? && object.banner.variable?
      object.banner.variant(resize_to_fill: [970, 250])
    else
      image_url("default-banner.png")
    end
  end
end