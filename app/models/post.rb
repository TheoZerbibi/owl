class Post < ApplicationRecord
  has_rich_text :post
  belongs_to :user
end
