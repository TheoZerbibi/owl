class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :omniauthable

  has_one_attached :avatar
  has_one_attached :banner
  has_person_name

  extend FriendlyId
  friendly_id :id, use: :slugged

  has_many :notifications, as: :recipient
  has_many :services
  has_many :posts
  acts_as_followable
  acts_as_follower
end
