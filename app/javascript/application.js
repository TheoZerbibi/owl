// Entry point for the build script in your package.json
import "./controllers"
import "@hotwired/turbo-rails"
import * as bootstrap from "bootstrap"
import './channels/**/*_channel.js'
import "trix"
import "@rails/actiontext"
//= require rails-ujs
//= require jquery

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $(document.getElementById(input.id + "_medium")).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".banner").change(function(){
    alert("ici");
    readURL(this);
});

function myFunction()
{
    alert("WESH");
}
$(document).ready(myFunction);