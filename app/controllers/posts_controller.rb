class PostsController < ApplicationController

  def index
    redirect_to root_path
  end

  def show
    @post = Post.find(params[:id])
    @author = User.find(@post.user_id)
  end

  def create
    @post = current_user.posts.create(post_param)
    Pusher.trigger('posts-channel','new-post', {
      user_id: current_user.id,
      username: current_user.username,
      caption: @post.post.body
    })
    redirect_to root_path, notice: "Message Posted"
  end

  private
  def post_param
    params.require(:post).permit(:post)
  end
end
