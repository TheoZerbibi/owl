class HomeController < ApplicationController
  def index
    @post = Post.new
    @myPost = Announcement.order(created_at: :desc)
    @myPost += current_user.posts.order(created_at: :desc)
  end

  def terms
  end

  def privacy
  end
end
